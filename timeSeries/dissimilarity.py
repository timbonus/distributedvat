import numpy as np

# Accept as an argument an RDD of indexed time series.    
def dissimilarity(indexedTimeSeries,numSeries,metric='NCC'):
    
    def euclidean(row,col):
        return ((row[0], col[0]), np.linalg.norm(row[1] - col[1]))
    
    def NCC(row,col):
        return ((row[0], col[0]), 1 - np.max(np.fft.ifft(row[1] * np.conj(col[1]))))
    
    def dtw(row, col, dist = lambda x, y: abs(x - y)):
        x = row[1]
        y = col[1]
        r, c = len(x), len(y)
        D0 = np.zeros((r + 1, c + 1))
        D0[0, 1:] = np.inf
        D0[1:, 0] = np.inf
        D1 = D0[1:, 1:] # view

        for i in range(r):
            for j in range(c):
                D1[i, j] = dist(x[i], y[j])

        for i in range(r):
            for j in range(c):
                D1[i, j] += min(D0[i, j], D0[i, j+1], D0[i+1, j])

        return ((row[0], col[0]), D1[-1, -1] / sum(D1.shape))
    
    #def dtwRDDint(row,col):
    #dtwdist = dtwRDD(row[1],col[1],dist = lambda x, y: abs(x - y))
    #return ((row[0], col[0]), dtwdist)
    
    # z-normalise the time series inputs.
    znorm_rows = indexedTimeSeries.mapValues(lambda row: row - np.mean(row)/np.std(row))

    # Calculate Fourier transform of normalised inputs.
    #if NCC: ft_rows = znorm_rows.mapValues(lambda row: np.fft.fft(row/np.linalg.norm(row)))
    if (metric == 'NCC') | (metric == 'ncc'): 
        ft_rows = znorm_rows.mapValues(lambda row: np.fft.fft(row/np.linalg.norm(row)))
        
    # Generate upper triangle of matrix pairs using Cartesian product.
    if (metric == 'NCC') | (metric == 'ncc'):
        NCC_row = ft_rows.cartesian(ft_rows).map(lambda (row,col): NCC(row,col))
    elif metric.startswith('euclid'):
        NCC_row = znorm_rows.cartesian(znorm_rows).map(lambda (row,col):euclidean(row,col))
    elif (metric == 'DTW') | (metric == 'dtw'):
        NCC_row = znorm_rows.cartesian(znorm_rows).map(lambda (row,col):dtw(row,col))
    else:
        print 'Incorrect metric - default to NCC'
        NCC_row = ft_rows.cartesian(ft_rows).map(lambda (row,col):NCC(row,col))

    def init_array(col_val, numSeries=numSeries):
        (col, dissimilarity) = col_val
        init_row = np.zeros(numSeries)
        init_row[col] = dissimilarity
        return init_row

    def build_row(partial_row, next_col_val):
        (col, dissimilarity) = next_col_val
        partial_row[col] = dissimilarity
        return partial_row
    
    # This combines all rows by the row number and produces an array indexed by column number.
    NCC_rowmat = NCC_row.map(lambda ((row, col), dissimilarity): (row, (col, dissimilarity))
                            ).combineByKey(
                                init_array,build_row,(lambda partial_row1, partial_row2: partial_row1 + partial_row2))
    
    return NCC_rowmat