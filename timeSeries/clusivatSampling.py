import numpy as np

# Develops the sampled dissimilarity matrix for clusiVAT images.
def clusivatSampling(dissimRows, clustEstimate = 10, sampleRatio = 1.0, clusterCount = False):
    
    # Deteriming cluster seed points - distinguished objects.
    m = np.zeros(clustEstimate)  # index of distinguished points
    clusSeed = m[0]
    y1 = dissimRows.filter(lambda row: row[0] == clusSeed).collect() # Just collects first row.

    # Then we have this non-deterministic iteration which requires constant 
    # communication back to the RDD looking for the row corresponding to the 
    # last distinguished point that is required for the next distinguished point.
    for i in range(1,clustEstimate):
        y2 = dissimRows.filter(lambda x: x[0] == m[i-1]).collect()
        y_min = np.minimum(y1[0][1],y2[0][1])
        m[i] = np.argmax(y_min)   
        
    m = m.astype(int)
    
    if sampleRatio != 1.0: 
        # Chooses a random selection of points for each distinguished object.
        fractions = {i: sampleRatio for i in range(len(m))}

        # This sampling just selects each item with certain probability.
        # Fails to meet the criterium of selecting a proportional amount.
        dissimRowsSample = dissimRows.map(lambda (ind, row): (np.argmin(row[m]), (ind, row))
                                         ).sampleByKey(False,fractions)
    else:
        dissimRowsSample = dissimRows.map(lambda (ind, row): (np.argmin(row[m]), (ind, row)))
    
    if clusterCount:
        sampleCount = dissimRowsSample.combineByKey(
                                (lambda row: 1),
                                (lambda comb,row: comb+1),
                                (lambda comb1, comb2: comb1+comb2))
        print sampleCount.collect()

    # First filter the desired data to minimise data sent back to driver program.
    dissimRowsSampleData = dissimRowsSample.map(lambda (clus, row): (row[0],row[1])).collect()

    # Sort the returned list of tuples by the row index.
    dissimRowsSampleData.sort(key = lambda tup: tup[0])
    
    # Define an index for selecting the columns to match the sampled rows indices.
    sampleIndex = np.array([i[0] for i in dissimRowsSampleData])    
    
    # This collates the rows into a numpy array producing the final sampled dissimilarity matrix.
    dissimRowsSampleMatrix = np.vstack(tuple(i[1][sampleIndex] for i in dissimRowsSampleData))
    
    return sampleIndex, dissimRowsSampleMatrix