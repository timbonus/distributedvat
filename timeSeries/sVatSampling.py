import numpy as np

# Develops the sampled dissimilarity matrix for clusiVAT images.
def sVatSampling(timeSeriesRDD, numSeries, metric='NCC', clustEst = 10, sampleRatio = 1.0, clusterCount = False):
    
    def dtw(row, col, dist = lambda x, y: abs(x - y)):
        x = row[1]
        y = col[1]
        r, c = len(x), len(y)
        D0 = np.zeros((r + 1, c + 1))
        D0[0, 1:] = np.inf
        D0[1:, 0] = np.inf
        D1 = D0[1:, 1:] # view

        for i in range(r):
            for j in range(c):
                D1[i, j] = dist(x[i], y[j])

        for i in range(r):
            for j in range(c):
                D1[i, j] += min(D0[i, j], D0[i, j+1], D0[i+1, j])

        return ((row[0], col[0]), D1[-1, -1] / sum(D1.shape))
    
    def row_dist(rows,metric,row_ts):
        if (metric == 'NCC') | (metric == 'ncc'):
            return lambda col: (1 - np.max(np.fft.ifft(row_ts[0][1] * np.conj(col))))
        elif metric.startswith('euclid'):
            return lambda col: np.linalg.norm(row_ts[0][1] - col[1])
        elif (metric == 'DTW') | (metric == 'dtw'):
            return lambda col: dtw(row_ts[0][1],col)
        else:
            print 'Incorrect metric - default to NCC'
            return lambda col: (1 - np.max(np.fft.ifft(row_ts[0][1] * np.conj(col))))
    
    def calculate_row(m,rows,metric=metric,numSeries=numSeries):
        row_ts = rows.filter(lambda row: row[0] == m).collect()
        NCC_row = rows.mapValues(row_dist(rows,metric,row_ts))
        row_return = NCC_row.collect()
        init_row = np.zeros(numSeries)
        for i in row_return:
            init_row[i[0]] = i[1]
        return init_row
    
    # z-normalise the time series inputs.
    znormRDD = timeSeriesRDD.mapValues(lambda row: row - np.mean(row)/np.std(row))

    if (metric == 'ncc') | (metric == 'NCC'):
        # Transform to Fourier transform.
        rows = znormRDD.mapValues(lambda row: np.fft.fft(row/np.linalg.norm(row)))
    else:
        rows = znormRDD
    
    # Initialise the algorithm by choosing the first instance / row.
    m = np.zeros(clustEst)  # index of distinguished points
    dissim_rows = np.zeros([clustEst,numSeries])  # index of distinguished points
    clus_seed = m[0]
    dissim_rows[0] = calculate_row(clus_seed,rows) # Just collects first row.
    # Determines the second distinguished point before passing to iteration loop.
    m[1] = np.argmax(dissim_rows[0])          
    dissim_rows[1] = calculate_row(m[1],rows)

    # Then we have this non-deterministic iteration which requires constant 
    # communication back to the RDD looking for the row corresponding to the 
    # last distinguished point that is required for the next distinguished point.
    # Demonstrate that this part is actually dependent on that parameter selection.
    # Entirely dependent on the communications of this algorithm.
    # Use the first two rows to generate distinguished instance index array, m.
    for i in range(1,clustEst-1):
        y_min = np.minimum(dissim_rows[i-1],dissim_rows[i])
        m[i+1] = np.argmax(y_min)
        dissim_rows[i+1] = calculate_row(m[i+1],rows)

    u_m, u_ind = np.unique(m,return_index=True)

    u_m = u_m.astype(int)
    dissim_rows = dissim_rows[u_ind]

    protoClust = dissim_rows.argmin(axis=0)

    # Determine a sampled index for calculating the siVAT image.
    sampleIndex = np.array([]).reshape(1,0)
    # Determine the cluster allocations.
    for i in enumerate(u_m):
        m_ind = np.where(protoClust == i[0])
        m_cnt = np.ceil(len(m_ind[0])*sampleRatio)
        sampleIndex =  np.append(sampleIndex, np.random.choice(m_ind[0],m_cnt,replace=False))
    
    sampledRows = rows.filter(lambda x: x[0] in sampleIndex).map(lambda row: (np.where(sampleIndex == row[0])[0][0], row[1]))
        
    return sampledRows, sampleIndex.astype(int), protoClust