# This is an attempt to mimic the MATLAB implementation.

import numpy as np

def VAT(D):
    N = len(D)
    # Define the set of row indices available for selection in terms of 
    # boolean index mask. Initally all available.
    # J represents the rows available to add to the MST.
    J = np.array(range(N))
    
    # Define MST connection array.
    C = np.zeros(N, dtype=int)
    
    # Define MST edge weight array.
    d = np.zeros(N, dtype=float)
    
    # Define index array for reordering.
    I = np.array([], dtype=int)
    
    # Find coordinates of maximum value in D. 
    colmax = np.max(D,axis=0)
    j = np.argmax(colmax) # Index of max column.
    i = np.argmax(D[:,j])   # Index of max row.
    
    # Assign row of maximum value to the first element of reordering index.
    I = np.append(I,i)
    
    # Remove that row from further assessment.
    J = np.delete(J,i)
    
    # MST connection array already defined with ones.
    # First element of MST (C[0] = 1) says nothing, really.
    # Seond element of MST (C[1] = 1) says that the second node in the development of the
    # MST is connected directly to the first.
    # C[n] shows the node to which the (n-1)th node is conncted (where C[n] < N).
    # c[n] is row (node in the MST) which the current node is closest to (min dissimilarity
    # in the masked dissimilarity matrix.)
    
    # Generate remaining elements of VAT reordering.
    for ind in range(1,N):
        colmin = np.min(D[np.ix_(I,J)],axis=0)
        j = np.argmin(colmin)
        i = np.argmin(D[I,J[j]])    # Index of min row.
        d[ind] = D[I[i],J[j]]      # Actual minimum distance to connect to MST.
        
        I = np.append(I,J[j])
        J = np.delete(J,j)
        C[ind] = i
    
    D = D[np.ix_(I,I)]
    
    return D, I, C, d