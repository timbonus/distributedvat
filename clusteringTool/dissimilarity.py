import numpy as np

# Accept as an argument an RDD of indexed time series.    
def dissimilarity(indexedData,numInstances,catFeats=None,metric='catOverlap'):
    
    # contCols specifies the columns in each instance that
    if catFeats is None:
        contFeats = np.arange(len(indexedData.take(1)[0][1]))
        catFeats = np.array([])
        metric = 'euclidean'
    else:
        contFeats = np.setdiff1d(np.arange(len(indexedData.take(1)[0][1])),catFeats)
        #catFeats = np.setdiff1d(contFeats, np.arange(len(indexedData.take(1)[0][1])))
    
    def euclidean(row,col):
        return ((row[0], col[0]), np.linalg.norm(row[1] - col[1]))
    
    def catOverlap(row,col):
        # For categorical features, similarity can be {0,1}.
        return ((row[0], col[0]), np.linalg.norm(row[1][contFeats] - col[1][contFeats]) + 
                len(catFeats) - np.sum(np.equal(row[1][catFeats],col[1][catFeats])))
    
    def catGoodall3(row,col,catDictList):
        # For categorical features, similarity depends on the nature of the distribution.
        return ((row[0], col[0]), np.linalg.norm(row[1][contFeats] - col[1][contFeats]) + len(catFeats) - 
                np.sum([(1-catDictList[catInd][row[1][cat]])**2*\
                        np.equal(row[1][cat],col[1][cat]) for catInd, cat in enumerate(catFeats)]))
    
    # def mutualInformation(row,col):
    # REFERENCES:
    # https://en.wikipedia.org/wiki/Mutual_information
      
    # Generate Cartesian product - basis of dissimilarity matrix.
    if metric == 'euclidean':
        dissimPairs = indexedData.cartesian(indexedData).map(lambda (row,col):euclidean(row,col))
    elif metric == 'catOverlap':
        dissimPairs = indexedData.cartesian(indexedData).map(lambda (row,col):catOverlap(row,col))
    elif metric == 'catGoodall3':
        catDictList = []
        for cat in catFeats:
            catLabels = sorted(indexedData.map(lambda col: col[1][cat]).distinct().collect())
            catLabels.append(catLabels[-1]+1)
            _, catHist = indexedData.map(lambda col: col[1][cat]).histogram(catLabels)
            catDictList.append({j: catHist[i]*1.0/numInstances for i,j in enumerate(catLabels[:-1])})
        catDictList = np.array(catDictList)
        print catDictList
        dissimPairs = indexedData.cartesian(indexedData).map(lambda (row,col):catGoodall3(row,col,catDictList=catDictList))
    else:
        print 'Incorrect metric - default to euclidean'
        dissimilarity = indexedData.cartesian(indexedData).map(lambda (row,col):euclidean(row,col))

    def init_array(col_val, numInstances=numInstances):
        (col, dissimilarity) = col_val
        init_row = np.zeros(numInstances)
        init_row[col] = dissimilarity
        return init_row

    def build_row(partial_row, next_col_val):
        (col, dissimilarity) = next_col_val
        partial_row[col] = dissimilarity
        return partial_row
    
    # This combines all rows by the row number and produces an array indexed by column number.
    dissimRows = dissimPairs.map(lambda ((row, col), dissimilarity): (row, (col, dissimilarity))
                            ).combineByKey(
                                init_array,build_row,(lambda partial_row1, partial_row2: partial_row1 + partial_row2))
    
    return dissimRows