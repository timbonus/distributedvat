import os
import numpy as np
from pyspark import SparkContext, SparkConf
import inspect
from sklearn import metrics

class iVAT_image:
    """
    A class representing an iVAT image for single linkage clustering.
    
    Each instance possesses attributes representing the VAT Minimum Spanning Tree, 
    VAT and iVAT images associated with a batched input data set. Additonal attributes
    describe the single linkage clusters observed in that image.
    
    Kyoto network data can be imported from a text file by passing a path string, or 
    other data can be used by setting the :nwdata: attribute directly.
    """
    
    def __init__(self):
        """
        Instantiate an object of this class.
        
        Initally all attributes are None type, but can be populated by the class methods.
        
        Attributes:
        data (numpy.ndarray): Input data. A two-dimensional numpy array - axis 1 representing 
                                instances, and axis 0 representing features. Features can be 
                                numerical or categorical, however categorical features must be 
                                mapped to a numerical value.
        normValues (tuple): Z-normalisation parameters (mean,stdev) for data attribute. 
                            Optionally set by self.normalisation.
        sessionDataRDD (pyspark.RDD): RDD of key-value pairs of instances in original data set.
        normSessionDataRDD (pyspark.RDD): Output of self.normalisation function.
        label (dict): Optional classification labels for original instances, with dictionary key 
                      corresponding to key of sessionDataRDD.
        sampleIndex (np.ndarray): Index of original data set sampled by sVAT process.
        D_VAT (numpy.ndarray): VAT reordered dissimilarity matrix. Set by self.iVAT().
        D_iVAT (numpy.ndarray): iVAT reordered dissimilarity matrix. Set by self.iVAT().
        I_VAT (numpy.ndarray): VAT reordering index. Set by self.iVAT().
        C (numpy.ndarray): MST for devloping VAT. C[n] shows the node to which the (n-1)th node 
                           is conncted. Set by self.iVAT().
        d (numpy.ndarray): Edge weight for each node added to VAT MST. Set by self.iVAT().
        silScores (numpy.ndarray): Silhouette scores for each instance in the data batch, for
                                   a given clustering. Set by self.silhouette()
        topClusterAttr (dict): Dictionary containing the bounds of the largest N clusters
                                 for a given single linkage partitoning of the VAT reordered
                                 data. 
        """
        self.data = None
        self.normValues = None
        self.sessionDataRDD = None
        self.normSessionDataRDD = None
        self.label = None
        self.sampleIndex = None
        self.catDomain = None
        self.D = None
        self.D_VAT = None
        self.D_iVAT = None
        self.I_VAT = None
        self.C = None
        self.d = None
        self.scalingFactor = None
        self.silScores = None
        self.topClusterAttr = None

    # Pass the RDD, the columns on which to act and the type of normalisation.
    # If mean / stdev previously calculated they can be passed as optional argument
    # as a tuple (mean, stdev).
    # normValues requests that the mean and stdev are returned for numerical features
    # for use on another data set.
    def normalisation(
        self,
        dataRDD, 
        cols=None, 
        normalise='standardWithCat',
        catFeats=None, 
        meanStDev=None, 
        normValues=False
    ):
        """
        Performs normalisation of features in a numeric data set of key-value pairs in a Spark RDD.
        
        Normalisation of features performed through substraction of mean and division by standard
        deviation. Input RDD is mapped to an output data RDD with no change to the instance indices.
        Mean and stdev can be calculated from input data or can be provided as an argument to allow
        for consistent normalisation if comparing multiple data sets. Option to return (mean, stdev)
        parameters to provide to another data set.
        
        This method can also perform a binarising pre-processing step on specified categorical features 
        where there is a need to perform centroid calculations involving those categorical features.
        This option will increase the number of features of all instances and NAIVELY ASSUMES THAT A 
        GIVEN DATA BATCH WILL CONTAIN ALL POSSIBLE CATEGORIES. Care must be taken using this option
        as datasets containing different category domains will NOT be comparable.
        
        Arguments: 
        dataRDD (pyspark.RDD): Input data contained in a Spark Resilient Distributed Dataset (RDD).
        cols (numpy.ndarray): Column indices to normalise.
        normalise (string): Can be 'standard' or 'standardWithCat'. The 'standard' option normalises
                            all features in cols, treating all features as numeric features, including 
                            categorical features. The 'standardWithCat' option normalises all features
                            specified in col, as in 'standard' option, except the feature columns
                            specified in catFeats. The domain of categories is determined from the 
                            data batch, with each categorical feature column removed, binarised,
                            and appended as a set of columns at the end of each instance. 
        catFeats (list): Indicates the column numbers of the input data containing categorical features.
        meanStDev (tuple): Optional user-specified (mean, stdev) to use for normalisation of numeric
                           features.
        normValues (bool): Optional flag to indicate if tuple containing (mean, stdev) should be
                             returned.
                             
        Returns:
        catFeats (list): Indices of columns of categorical features in the output data set. Note that
                         for binarised categorical features this is changed to None to facilitate
                         interworking with iVAT_image.dissimilarity().
        normDataRDD (pyspark.RDD): An RDD containing the normalised data mapping.
        meanStDev (tuple): Optional tuple of arrays (mean, stdev) containing the mean and stdev of 
                           each feature.
        """
        # Check if RDD cols to normalise are specified. If not, all cols to be normalised.
        if cols == None:
            cols = np.arange(len(dataRDD.take(1)[0][1]))

        # Checks if 'standardWithCat' option is being used in conjunction with a necessary
        # list of categorical features and if not the 'standard' option is used.
        if (normalise == 'standardWithCat') & (type(catFeats) == None):
            print 'No category features specified. Defaulting to standard normalisation.'
            normalise='standard'

        # Check if standard parameters are specified, else calculate them.
        if meanStDev == None:
            mean_cols = dataRDD.map(lambda col: col[1]).mean()
            stdev_cols = dataRDD.map(lambda col: col[1]).stdev()
        else:
            mean, stdev = meanStDev
            mean_cols = mean
            stdev_cols = stdev

        # Define normalisation functions to be used in MapReduce implementation.
        # Standard norm calculates standard score for all elements in each column.
        def standardNorm(row):
            row[cols] = (row[cols] - mean_cols[cols])/stdev_cols[cols]
            row[cols] = np.nan_to_num(row[cols])
            return row

        # Binarising norm performs standard normalisation on numerical columns, while
        # binarised categorical feature columns and appended as multi-column binarised 
        # features based on the catLabels argument. 
        def dataBinaryNorm(row,catLabels):
            row[cols] = (row[cols] - mean_cols[cols])/stdev_cols[cols]
            row[cols] = np.nan_to_num(row[cols])
            rowCats = row[catFeats]
            row = np.delete(row,catFeats)
            meanStDev = mean_cols, stdev_cols
            for catInd, catValue in enumerate(rowCats):
                row = np.append(row,catLabels[catInd] == catValue)
            return row

        # Perform requested normalisation and return normalised RDD along with
        # updated catFeats column and normalisation parameters if requested.
        if normalise == 'standard':
            normDataRDD = dataRDD.mapValues(lambda row: standardNorm(row))
            if normValues:
                return catFeats, None, meanStDev, normDataRDD
            else:
                return catFeats, None, None, normDataRDD
        elif normalise == 'standardWithCat': 
            # Produces a list of numpy.ndarray objects containing the list of 
            # unique labels for each of the categorical features specified in 
            # the datasets.
            cols = np.setdiff1d(cols,catFeats)
            catLabels = []
            for cat in catFeats:
                catLabels.append(sorted(dataRDD.map(
                            lambda col: col[1][cat]).distinct().collect()))
            
            # List of categorical labels passed to dataBinaryNorm returning 
            # normalised instances with binarised features appended.
            normDataRDD = dataRDD.mapValues(
                lambda row: dataBinaryNorm(row,catLabels))
            if normValues:
                return None, catLabels, meanStDev, normDataRDD
            else:
                return None, catLabels, None, normDataRDD
        else:
            # Warn if valid normalisation not specified or performed.
            print 'No valid normalisation specified. No normalisation performed.'
            if normValues:
                return catFeats, catLabels, meanStDev, normDataRDD
            else:
                return catFeats, catLabels, None, normDataRDD

    def sVatSampling(
        self,
        indexedData, 
        metric='euclidean', 
        clustEst = 10, 
        sampleRatio = 1.0
    ):
        """
        Performs downsampling of a large Spark data set in line with sVAT algorithm, returning the sample.
        

        """
        
        # Three distance/dissimilarity metrics are defined for the MapReduce implementation.
        # Euclidean distance calculates L2 norm between the two instances.
        def euclidean(row,col):
            return ((row[0], col[0]), np.linalg.norm(row[1] - col[1]))

        # Category overlap distance calculates L2 norm between the numerical features of the 
        # two instances and adds 1 for each non-matching categorical feature.
        def catOverlap(row,col):
            # For categorical features, similarity can be {0,1}.
            return ((row[0], col[0]), np.linalg.norm(row[1][contFeats] - col[1][contFeats]) + 
                    len(catFeats) - np.sum(np.equal(row[1][catFeats],col[1][catFeats])))

        # Goodall3 categorical distance calculates L2 norm between the numerical features of the 
        # two instances while adding an additional value dependent on the probability distribution
        # of the categories within each categorical feature as described in the docstring reference.
        # This function accepts a list of dictionaries describing the probability distribution of
        # each categorical feature.
        def catGoodall3(row,col,catDictList):
            # For categorical features, similarity depends on the nature of the distribution.
            return ((row[0], col[0]), np.linalg.norm(row[1][contFeats] - col[1][contFeats]) + 
                    len(catFeats) - np.sum([(1-catDictList[catInd][row[1][cat]])**2*\
                            np.equal(row[1][cat],col[1][cat]) for catInd, cat in enumerate(catFeats)]))
        
        def calculateRow(m,rows,numInstances,metric=metric):
            rowSelect = rows.filter(lambda row: row[0] == m).collect()
            if metric == 'euclidean':
                NCC_row = rows.mapValues(lambda row: euclidean(row,rowSelect[0]))
            elif metric == 'catOverlap':
                NCC_row = rows.mapValues(lambda row: catOverlap(row,rowSelect[0]))
            elif metric == 'catGoodall3':
                print 'FAIL'
            else:
                # If no metric specified Euclidean distance is applied, though this can give misleading 
                # results where categorical features exist.
                print 'Incorrect metric - default to euclidean'
                NCC_row = rows.mapValues(lambda row: euclidean(row,rowSelect[0]))
                
            rowReturn = NCC_row.collect()
            initRow = np.zeros(numInstances)
            for i,j in rowReturn:
                initRow[i] = j
            return initRow
        
        # Number of instances in the batch required for MapReduce implementation of dissimilarity matrix
        # development.
        numInstances = indexedData.count()
        
        # Initialise the algorithm by choosing the first instance / row.
        m = np.zeros(clustEst)  # index of distinguished points
        dissimRows = np.zeros([clustEst,numInstances])  # empty array for distinguished instances
        clus_seed = m[0]
        dissimRows[0] = calculateRow(clus_seed,indexedData,numInstances) # Just collects first row.
        # Determines the second distinguished point before passing to iteration loop.
        m[1] = np.argmax(dissimRows[0])          
        dissimRows[1] = calculateRow(m[1],indexedData,numInstances)

        # Then we have this non-deterministic iteration which requires constant 
        # communication back to the RDD looking for the row corresponding to the 
        # last distinguished point that is required for the next distinguished point.
        # Demonstrate that this part is actually dependent on that parameter selection.
        # Entirely dependent on the communications of this algorithm.
        # Use the first two rows to generate distinguished instance index array, m.
        for i in range(1,clustEst-1):
            y_min = np.minimum(dissimRows[i-1],dissimRows[i])
            m[i+1] = np.argmax(y_min)
            dissimRows[i+1] = calculateRow(m[i+1],indexedData,numInstances)

            u_m, u_ind = np.unique(m,return_index=True)

            u_m = u_m.astype(int)
            dissimRows = dissimRows[u_ind]

            protoClust = dissimRows.argmin(axis=0)

            # Determine a sampled index for calculating the siVAT image.
            sampleIndex = np.array([]).reshape(1,0)
            # Determine the cluster allocations.
            for i in enumerate(u_m):
                m_ind = np.where(protoClust == i[0])
                m_cnt = np.ceil(len(m_ind[0])*sampleRatio).astype(int)
                sampleIndex =  np.append(sampleIndex, np.random.choice(m_ind[0],m_cnt,replace=False))

                sampledData = rows.filter(
                    lambda x: x[0] in sampleIndex).map(
                    lambda row: (np.where(sampleIndex == row[0])[0][0], row[1]))

        return sampledData, sampleIndex.astype(int), protoClust
            
    def dissimilarity(
        self,
        indexedData,
        catFeats=None,
        metric='euclidean',
        sampleRatio = 1.0,
        clustEst = 10
    ):
        """
        Maps a Spark RDD of key-value pairs to its dissimilarity matrix and returns the matrix RDD.
        
        Accepts an RDD of key-value pairs, with instances of a data set represented by numeric values.
        The dissimilarity matrix can be calculated from a mixture of numeric and categorical features
        determined by the catFeats indices and metric specified.
        
        Arguments:
        indexedData (pyspark.RDD): RDD of key-value pairs, with consecutive integer keys (starting from
                                   0) indexing each instance.
        catFeats (list): Indices of columns containing categorical features (optional)
        metric (string): Dissimilarity metric choice. Options are-
                            - 'euclidean' which applies Euclidean distance to all features, 
                            - 'catOverlap' which applies Euclidean distance to all columns not in 
                              catFeats, while contributing 0 distance for each matching catFeats 
                              columns and 1 for each non-matching catFeats column, 
                            - 'catGoodall3 which, in addition to Euclidean distance for numeric 
                              features implements Goodal3 categorical distance, a probabilistic 
                              overlap value, with matching categorical values having non-zero 
                              dissimilarity, with more uncommon categorical values having lower 
                              dissimilarity than more common categorical values (Boriah et al, 
                              'Similarity Measures for Categorical Data: A Comparative Evaluation', 
                              Proceedings of the 2008 SIAM International Conference on Data Mining.)
        sampleRatio (float): Used to control sampling of input data if sVAT approximation for Big
                             data sets is being used, where sampleRatio is an approximate measure of
                             the number of instances in the sampled data set to the original number of 
                             samples in indexedData. Can take value 0.0 < sampleRatio <= 1.0 where
                             1.0 indicates that no sampling is performed.
        clustEst (int): An initial overestimate of the expected number of clusters in the data, 
                        required by the sVAT algorithm.
        
        Returns:
        dissimRows (pyspark.RDD): An RDD of key-value pairs, where keys are the same as those of the
                                  input RDD and the value is an array of pairwise dissimilarity 
                                  values between the key index and all other instances in the data.
                                  The array is indexed using the same indices as the input RDD.
        """
        
        # Number of instances in the batch required for MapReduce implementation of dissimilarity matrix
        # development.
        numInstances = indexedData.count()
        
        # Check if any categorical features are specified to allow filtering of numerical features.
        if catFeats is None:
            contFeats = np.arange(len(indexedData.take(1)[0][1]))
            catFeats = np.array([])
            metric = 'euclidean'
        else:
            contFeats = np.setdiff1d(np.arange(len(indexedData.take(1)[0][1])),catFeats)

        # Three distance/dissimilarity metrics are defined for the MapReduce implementation.
        # Euclidean distance calculates L2 norm between the two instances.
        def euclidean(row,col):
            return ((row[0], col[0]), np.linalg.norm(row[1] - col[1]))

        # Category overlap distance calculates L2 norm between the numerical features of the 
        # two instances and adds 1 for each non-matching categorical feature.
        def catOverlap(row,col):
            # For categorical features, similarity can be {0,1}.
            return ((row[0], col[0]), np.linalg.norm(row[1][contFeats] - col[1][contFeats]) + 
                    len(catFeats) - np.sum(np.equal(row[1][catFeats],col[1][catFeats])))

        # Goodall3 categorical distance calculates L2 norm between the numerical features of the 
        # two instances while adding an additional value dependent on the probability distribution
        # of the categories within each categorical feature as described in the docstring reference.
        # This function accepts a list of dictionaries describing the probability distribution of
        # each categorical feature.
        def catGoodall3(row,col,catDictList):
            # For categorical features, similarity depends on the nature of the distribution.
            return ((row[0], col[0]), np.linalg.norm(row[1][contFeats] - col[1][contFeats]) + 
                    len(catFeats) - np.sum([(1-catDictList[catInd][row[1][cat]])**2*\
                            np.equal(row[1][cat],col[1][cat]) for catInd, cat in enumerate(catFeats)]))
        

        def calculateRow(m,rows,numInstances,metric=metric):
            rowSelect = rows.filter(lambda row: row[0] == m).collect()
            if metric == 'euclidean':
                NCC_row = rows.mapValues(lambda row: euclidean(row,rowSelect[0]))
            elif metric == 'catOverlap':
                NCC_row = rows.mapValues(lambda row: catOverlap(row,rowSelect[0]))
            elif metric == 'catGoodall3':
                print 'FAIL'
            else:
                # If no metric specified Euclidean distance is applied, though this can give misleading 
                # results where categorical features exist.
                print 'Incorrect metric - default to euclidean'
                NCC_row = rows.mapValues(lambda row: euclidean(row,rowSelect[0]))

            rowReturn = NCC_row.collect()
            initRow = np.zeros(numInstances)
            for i,j in rowReturn:
                initRow[i] = j
            return initRow
        
        if sampleRatio != 1.0:
            # Initialise the algorithm by choosing the first instance / row.
            m = np.zeros(clustEst)  # index of distinguished points
            dissimRows = np.zeros([clustEst,numInstances])  # empty array for distinguished instances
            clus_seed = m[0]
            dissimRows[0] = calculateRow(clus_seed,indexedData,numInstances) # Just collects first row.
            # Determines the second distinguished point before passing to iteration loop.
            m[1] = np.argmax(dissimRows[0])          
            dissimRows[1] = calculateRow(m[1],indexedData,numInstances)

            # Then we have this non-deterministic iteration which requires constant 
            # communication back to the RDD looking for the row corresponding to the 
            # last distinguished point that is required for the next distinguished point.
            # Demonstrate that this part is actually dependent on that parameter selection.
            # Entirely dependent on the communications of this algorithm.
            # Use the first two rows to generate distinguished instance index array, m.
            for i in range(1,clustEst-1):
                y_min = np.minimum(dissimRows[i-1],dissimRows[i])
                m[i+1] = np.argmax(y_min)
                dissimRows[i+1] = calculateRow(m[i+1],indexedData,numInstances)

                u_m, u_ind = np.unique(m,return_index=True)

                u_m = u_m.astype(int)
                dissimRows = dissimRows[u_ind]

                protoClust = dissimRows.argmin(axis=0)

                # Determine a sampled index for calculating the siVAT image.
                sampleIndex = np.array([]).reshape(1,0)
                # Determine the cluster allocations.
                for i in enumerate(u_m):
                    m_ind = np.where(protoClust == i[0])
                    m_cnt = np.ceil(len(m_ind[0])*sampleRatio).astype(int)
                    sampleIndex =  np.append(sampleIndex, np.random.choice(m_ind[0],m_cnt,replace=False))

                    sampledData = rows.filter(
                        lambda x: x[0] in sampleIndex).map(
                        lambda row: (np.where(sampleIndex == row[0])[0][0], row[1]))

            #return sampledData, sampleIndex.astype(int), protoClust
            indexedData = sampledData
            numInstances = len(sampleIndex)
            self.sampleIndex = sampleIndex
        
        # Generate Cartesian product to form the basis of the dissimilarity matrix.
        # Map the ordered pairs as (row,col) to pairwise dissimilarity value.
        # MapReduce implementation applies this to the whole Cartesian product of data set with itself.
        if metric == 'euclidean':
            dissimPairs = indexedData.cartesian(indexedData).map(lambda (row,col):euclidean(row,col))
        elif metric == 'catOverlap':
            dissimPairs = indexedData.cartesian(indexedData).map(lambda (row,col):catOverlap(row,col))
        elif metric == 'catGoodall3':
            # catGoodall3 option requires the calculation of distributions of categories within each
            # categorical feature to pass to the catGoodall3 distance function.
            catDictList = []
            for cat in catFeats:
                catLabels = sorted(indexedData.map(lambda col: col[1][cat]).distinct().collect())
                catLabels.append(catLabels[-1]+1)
                _, catHist = indexedData.map(lambda col: col[1][cat]).histogram(catLabels)
                catDictList.append({j: catHist[i]*1.0/numInstances for i,j in enumerate(catLabels[:-1])})
            catDictList = np.array(catDictList)
            dissimPairs = indexedData.cartesian(indexedData).map(
                lambda (row,col):catGoodall3(row,col,catDictList=catDictList))
        else:
            # If no metric specified Euclidean distance is applied, though this can give misleading 
            # results where categorical features exist.
            print 'Incorrect metric - default to euclidean'
            dissimilarity = indexedData.cartesian(indexedData).map(lambda (row,col):euclidean(row,col))
        
        # Two stage MapReduce algorithm implements combineByKey function from pyspark.
        # This function uses a specified secondary function to combine all key-value pairs with the same 
        # key into a single key-value pair, where the values are combined using the secondary function.
        
        # In this implementation each key indexes a row in the dissimilarity matrix, with all instances
        # with that key combined by a secondary key into a numpy array, where the secondary key dictates
        # the column position of the associated dissimilarity value. In this way all the (row,col) keys
        # produced by the Cartesian product above, associated with the dissimilarity vlaue between those
        # two data instances, can be mapped to rows of the dissmilarity matrix.
        
        # The first helper function is used whenever a new row key is encountered, in which case a new
        # numpy array is initalised with zero values, and the col value of that array is set as the
        # dissimilarity value of this key-value pair.
        def init_array(col_val, numInstances=numInstances):
            (col, dissimilarity) = col_val
            init_row = np.zeros(numInstances)
            init_row[col] = dissimilarity
            return init_row
        
        # The second helper function is used whenever a new instance is encountered that has the same 
        # key as a previously initailised row array. The dissimilarity value of this instances is 
        # added to the row at the col position.
        def build_row(partial_row, next_col_val):
            (col, dissimilarity) = next_col_val
            partial_row[col] = dissimilarity
            return partial_row

        # This combines all rows with the same row key as developed on the separate nodes of the Spark
        # cluster. Numpy arrays have element-wise addition, so simply adding the consituent rows for
        # each row key will produce the final complete dissimilarity array for this row.
        dissimRows = dissimPairs.map(lambda ((row, col), dissimilarity): (row, (col, dissimilarity))
                                ).combineByKey(init_array,build_row,(
                                    lambda partial_row1, partial_row2: partial_row1 + partial_row2))

        # The combined RDD containing key-value pairs of row index and dissimilarity row for each of
        # the rows in the dissimilarity matricx is returned.
        return dissimRows

    def VAT(
        self,
        D
    ):
        """
        Performs VAT reordering of a dissimilarity matrix returning the image and ordering info.
        
        This is a Numpy implementation based on MATLAB source code written by T. Havens.
        http://www.ece.mtu.edu/~thavens/research.html.
        """
        N = len(D)
        # Define the set of row indices available for selection in terms of 
        # boolean index mask. Initally all available.
        # J represents the rows available to add to the MST.
        J = np.array(range(N))

        # Define MST connection array.
        C = np.zeros(N, dtype=int)

        # Define MST edge weight array.
        d = np.zeros(N, dtype=float)

        # Define index array for reordering.
        I = np.array([], dtype=int)

        # Find coordinates of maximum value in D. 
        colmax = np.max(D,axis=0)
        j = np.argmax(colmax) # Index of max column.
        i = np.argmax(D[:,j])   # Index of max row.

        # Assign row of maximum value to the first element of reordering index.
        I = np.append(I,i)

        # Remove that row from further assessment.
        J = np.delete(J,i)

        # MST connection array already defined with ones.
        # First element of MST (C[0] = 1) says nothing, really.
        # Seond element of MST (C[1] = 1) says that the second node in the development of the
        # MST is connected directly to the first.
        # C[n] shows the node to which the (n-1)th node is conncted (where C[n] < N).
        # c[n] is row (node in the MST) which the current node is closest to (min dissimilarity
        # in the masked dissimilarity matrix.)

        # Generate remaining elements of VAT reordering.
        for ind in range(1,N):
            colmin = np.min(D[np.ix_(I,J)],axis=0)
            j = np.argmin(colmin)
            i = np.argmin(D[I,J[j]])    # Index of min row.
            d[ind] = D[I[i],J[j]]       # Actual minimum distance to connect to MST.

            I = np.append(I,J[j])
            J = np.delete(J,j)
            C[ind] = i

        D = D[np.ix_(I,I)]

        return D, I, C, d

    def iVAT(
        self,
        D
    ):
        """
        Performs iVAT reordering of a dissimilarity matrix returning the image and ordering info.
        
        This is a Numpy implementation based on MATLAB source code written by T. Havens.
        http://www.ece.mtu.edu/~thavens/research.html.
        """
        N = len(D)

        D_VAT, I_VAT, C, d = self.VAT(D)

        D_iVAT = np.zeros(D_VAT.shape)

        # Simply iterate through rows and find the biggest cost to adding that node to the MST.
        for r in range(1,N):
            c = range(r)
            D_iVAT[r,c] = D_VAT[r,C[r]]
            c_no_self = np.delete(c,np.where(c == C[r]))
            D_iVAT[r,c_no_self] = np.max([D_iVAT[r,c_no_self],D_iVAT[C[r],c_no_self]],axis=0)
            D_iVAT[c,r] = D_iVAT[r,c].T

        return D_iVAT, D_VAT, I_VAT, C, d


    def data2VAT(self,
                 sc,
                 numSlices=4,
                 sampleRatio=1.0,
                 metric='catOverlap',
                 scaledSize = 100,
                 **kwargs):
        """
        Calculates iVAT image from input data and sets associated attributes of the iVAT_image class.
        
        The file2VAT function calls a number of helper functions in this class
        
        Calling file2VAT will create the VAT/iVAT image associated with this input. 
        
        The calculation comprises XXX steps:
        - Importing data. Possible from datapath and filename argument
        
        , as well as data associated with a start time and 
        duration of the batch to be analysed from that file. If the data is already assigned 
        these arguments can be passed dummy values. 
        
        The function uses the Spark Context obtained when the Notebook server is launched. As such
        SparkContext must be imported from the pyspark module in the application. The numSliced
        paraemeter dictates the parallelisation in the Spark cluster. sampleRatio
        
        
        All data is normalised using the normalisation function defined externally. The associated 
        kwargs can be used to 
        
            # kwargs = normalise,cols,meanStDev,catFeats,clustEst,
        
        
        """
        numSeries = len(self.data)
        lenSeries = len(self.data[0])

        sessionData = [(i, self.data[i,:-1]) for i in range(numSeries)]
        self.label = {i: self.data[i,-1] for i in range(numSeries)}

        self.sessionDataRDD = sc.parallelize(sessionData,numSlices=numSlices)        

        # Normalising the data.
        normalisationkwargs = set(inspect.getargspec(self.normalisation)[0]).intersection(kwargs.keys())
        catFeats, self.catDomain, self.normValues, self.normSessionDataRDD = self.normalisation(
            self.sessionDataRDD, normValues=True,**{c: kwargs[c] for c in normalisationkwargs})
        
        # Create RDD consisting of indexed dissimilarity arrays of each time series.
        dissimkwargs = set(inspect.getargspec(self.dissimilarity)[0]).intersection(kwargs.keys())
        tsDissimRDD = self.dissimilarity(
                    self.normSessionDataRDD,metric=metric,**{c: kwargs[c] for c in dissimkwargs})

        # Create dissimilarity matrix locally.
        dissimSampled = tsDissimRDD.collect()
        dissimSampled.sort(key = lambda tup: tup[0])
        self.D = np.vstack(tuple(i[1] for i in dissimSampled))

        # Produce iVAT image
        self.D_iVAT, self.D_VAT, self.I_VAT, self.C, self.d = self.iVAT(self.D)
        
        if scaledSize == None:
            self.scalingFactor = 1
        elif len(self.d) > (scaledSize*2 - 1):
            self.scalingFactor = len(self.d) / scaledSize
        else:
            self.scalingFactor = 1
    
    def silhouette(
        self,
        minClusCount,
        maxClusCount,
        clusStep = 1,
        mainClustersOnly = False,
        iVATdissim = False,
        minClusSize = 5
    ):
        """
        Calculates the silhouette score for a specified range of partitions, setting the attribute silScores.
        
        The silhouette index is a type of cluster validation index that is implemented in scikit-learn
        (http://scikit-learn.org/stable/modules/generated/sklearn.metrics.silhouette_score.html).
        
        Once iVAT reordering is performed, single linkage clustering into K clusters involves partitioning
        the VAT reordered index at the instances with the K-1 largest connecting edge weights, in self.d.
        Silhouette score can help determine the optimal K cluster partitions.
        
        This function wraps metrics.silhouette_score from scikit-learn and applies it to a range of single 
        linkage cluster partitions returning an array of scores. This allows the comparison of cluster validity
        for different partitions.
        
        Optional arguments allow outliers to be filtered based on the size of single-linkage clusters 
        that are generated. Removing outliers can give a more robust picture of the quality of clustering 
        structure.
        
        Optional argument allows the use of iVAT the graph-based dissimilarity values to be used in 
        assessing the Silhouette score. THIS IS NOT A TRUE SILHOUETTE SCORE.
        
        Arguments:
        minClusCount (int): A lower bound for the range of silhoette scores.
        maxClusCount (int):  An upper bound for the range of silhoette scores.
        iVATdissim  (bool): Optional argument indicating if iVAT dissimilarity values are used instead
                            of VAT dissimilarity.
        mainClustersOnly (bool): Optional argument indicating if outliers should be removed. Outliers
                                 are determined to be instances in single linkage clusters containing 
                                 less than minClusSize instances.
        minClusSize (int): Minimum cluster size to identify outliers in a given partition.
        
        Returns:
        None (silScores (numpy.ndarray) is set as an attribute of the instance).
        """
        
        # Initialises the silScore object, with clusters[i] holding the number of clusters
        # in the current partitioning and scores[i] holding the corresponding silhouette score
        # for that partition.
        self.silScores = {}
        self.silScores['clusters'] = np.arange(minClusCount,maxClusCount,clusStep)
        self.silScores['scores'] = np.zeros(len(self.silScores['clusters']))
        
        # Loops through the range of parition sizes to be checked.
        for Kind,K in enumerate(self.silScores['clusters']):
            # Identifies the partition points of the top K partitions.
            partindicesLoop = np.argsort(self.d)[-K:]
            partindicesLoop.sort()
            
            # Develops a cluster label array required for metrics.silhouette_score.
            labels = np.zeros(len(self.d))
            pointer = 0
            for pos, lab in enumerate(labels):
                if pointer == len(partindicesLoop):
                    labels[pos] = pointer + 1
                elif pos == partindicesLoop[pointer]:
                    labels[pos] = pointer
                    pointer += 1
                else:
                    labels[pos] = pointer

            # Check if outliers are to be filtered and obtains filtering index.
            if mainClustersOnly:
                mainClusters = []
                for clusLab in range(K+1):
                    if sum(labels == clusLab) >= minClusSize:
                        mainClusters.append(clusLab)
                mainData = np.array([pos for pos, lab in enumerate(labels) if lab in mainClusters])
            else:
                mainData = np.ones(len(self.d),dtype=bool)

            # Labels, filtering mask, and VAT/iVAT dissimilarity matrix are passed to silhoette function.
            if iVATdissim:
                self.silScores['scores'][Kind] = metrics.silhouette_score(
                    self.D_iVAT[mainData][:,mainData], labels=labels[mainData], metric="precomputed")
            else:
                self.silScores['scores'][Kind] = metrics.silhouette_score(
                    self.D_VAT[mainData][:,mainData], labels=labels[mainData], metric="precomputed")
        
        
    def topClusters(self,numClusters,numTopClusters):
        """
        Identifies the largest clusters for a given partition and sets their bounds as an attribute.
        
        For the iVAT reodering a specified number of the largest clusters from a specified single linkage
        partitioning are identified, and their upper and lower index bounds are saved set as an attribute
        of the class.
        
        Arguments:
        numClusters (int): The number of clusters to be partitioned.
        numTopClusters (int): The number of largest clusters to be identified.
                
        Returns:
        None (topClusterBounds (dict) is set as an attribute of the instance).
        """
        partindices = np.argsort(self.d)[-numClusters:]
        partindices.sort()
        partSizes = np.zeros(len(partindices))
        self.topClusterAttr = {}

        prevClust = 0

        for i,part in enumerate(partindices):
            partSizes[i] = part - prevClust
            prevClust = part
        
        bigClus = np.sort(np.argsort(partSizes)[-numTopClusters:])
        
        for i,topClus in enumerate(bigClus):
            self.topClusterAttr[i] = {}
            self.topClusterAttr[i]['bounds'] = (partindices[topClus-1],partindices[topClus])
            self.topClusterAttr[i]['centroid'] = self.normSessionDataRDD.filter(
                                                        lambda row: (row[0] > partindices[topClus-1]) & 
                                                                    (row[0] <= partindices[topClus])).map(
                                                            lambda row: row[1]).mean()