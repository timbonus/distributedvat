from VAT import VAT
import numpy as np

def iVAT(D):
    
    N = len(D)
    
    D_VAT, I_VAT, C, d = VAT(D)
    
    D_iVAT = np.zeros(D_VAT.shape)
    
    # Simply iterate through rows and find the biggest cost to adding that node to the MST.
    for r in range(1,N):
        c = range(r)
        D_iVAT[r,c] = D_VAT[r,C[r]]
        c_no_self = np.delete(c,np.where(c == C[r]))
        D_iVAT[r,c_no_self] = np.max([D_iVAT[r,c_no_self],D_iVAT[C[r],c_no_self]],axis=0)
        D_iVAT[c,r] = D_iVAT[r,c].T
        
    return D_iVAT, D_VAT, I_VAT, C, d