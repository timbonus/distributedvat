import numpy as np

# Pass the RDD, the columns on which to act and the type of normalisation.
# If mean / stdev previously calculated they can be passed as optional argument
# as a tuple (mean, stdev).
# normValues requests that the mean and stdev are returned for numerical features
# for use on another data set.
def normalisation(dataRDD, cols=None, normalise='standardWithCat', catFeats=None, meanStDev=None, normValues=False):
    
    if cols == None:
        cols = np.arange(len(dataRDD.take(1)[0][1]))
    
    if normalise == 'standardWithCat':
        if catFeats == None:
            print 'No category features specified. Defaulting to standard normalisation.'
            normalise='standard'
        else:
            cols = np.setdiff1d(cols,catFeats)
            catLabels = []
            for cat in catFeats:
                catLabels.append(sorted(dataRDD.map(lambda col: col[1][cat]).distinct().collect()))

    if meanStDev == None:
        mean_cols = dataRDD.map(lambda col: col[1]).mean()
        stdev_cols = dataRDD.map(lambda col: col[1]).stdev()
    else:
        mean, stdev = meanStDev
        mean_cols = mean
        stdev_cols = stdev
        
    def standardNorm(row):
        row[cols] = (row[cols] - mean_cols[cols])/stdev_cols[cols]
        row[cols] = np.nan_to_num(row[cols])
        return row
    
    def dataBinaryNorm(row):
        row[cols] = (row[cols] - mean_cols[cols])/stdev_cols[cols]
        row[cols] = np.nan_to_num(row[cols])
        rowCats = row[catFeats]
        row = np.delete(row,catFeats)
        meanStDev = mean_cols, stdev_cols
        for catInd, catValue in enumerate(rowCats):
            row = np.append(row,catLabels[catInd] == catValue)
        return row

    if normalise == 'standard':
        normDataRDD = dataRDD.mapValues(lambda row: standardNorm(row))
        if normValues:
            return catFeats, meanStDev, normDataRDD
        else:
            return catFeats, normDataRDD
    elif normalise == 'standardWithCat':
        normDataRDD = dataRDD.mapValues(lambda row: dataBinaryNorm(row))
        if normValues:
            return None, meanStDev, normDataRDD
        else:
            return None, normDataRDD
    else:
        print 'No valid normalisation specified. No normalisation performed.'
        if normValues:
            return catFeats, meanStDev, normDataRDD
        else:
            return catFeats, normDataRDD
    
    #if normalise == 'standardWithCat':
    